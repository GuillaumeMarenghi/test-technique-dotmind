import axios from "axios"

const getContacts = async (page: number) => {

    const response = await axios.get(`https://reqres.in/api/users?page=${page}&delay=1`)

    //return error status if error
    if(response.status != 200) return {error: response.status}

    //all contacts returned
    const contacts = response.data.data
    //pass snake_case to camelCase obj keys
    contacts.forEach(function (element:any) {
        element['firstname'] = element['first_name'];
        element['lastname'] = element['last_name'];
        delete element['first_name'];
        delete element['last_name'];     
    });

    //return array of contact with key in camelCase
    return contacts

};

export default getContacts;