export default interface Data {
    id: number,
    email: string,
    firstname: string,
    lastname: string,
    avatar: string,
}