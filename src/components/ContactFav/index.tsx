import React, {useState, useEffect} from "react";
import "./ContactFav.scss"
import Data from "../../types/data";
import ContactCard from "../ContactCard";

type AppProps = {
    favChange: number;
    setFavChange: (number: number) => void;
}

const ContactFav = (props: AppProps) => {

    //state allFavourite
    const [allFav, setAllFav] = useState<Array<Data> | null>()
    
    //get all favourites on local storage
    useEffect(()=>{
        const myLocalStorage = localStorage
        const regex = new RegExp(/[\w-\.]+@(reqres\.)+(in)$/)

        let getAllFav: Array<Data> = []
        for (const key in myLocalStorage) {
            if(regex.test(key)) getAllFav = [...getAllFav, JSON.parse(myLocalStorage[key])]
        }
        setAllFav(getAllFav)
        
    },[props.favChange])


    return(
        <div className="contact-fav-ctn">
            <div className="contact-fav-title">Favorites</div>
            <div className="contact-fav-empty">
                {allFav && allFav?.length == 0 && <div>empty for the moment</div>}
            </div>
            <div className="contact-fav-ctn-card">
                {allFav && allFav?.map(elm => <ContactCard key={elm.email} {...elm} {...props} />)}
            </div>
        </div>
    )
}

export default ContactFav;