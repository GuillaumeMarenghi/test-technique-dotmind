import React , {useState} from 'react';

import Title from '../Title';
import ContactList from '../ContactList';
import ContactFav from '../ContactFav';
import './App.scss';

function App() {

  //state to detect a change in local storage for the contactFav component
  const [favChange, setFavChange] = useState<number>(0)

  return (
    <div className="App">
      <Title />
      <div className="flex-content">
        <ContactList favChange={favChange} setFavChange={setFavChange}/>
        <ContactFav favChange={favChange} setFavChange={setFavChange}/>
      </div>
    </div>
  );
}

export default App;