import React, {useEffect, useState} from "react";
import "./ContactList.scss"
import Data from "../../types/data"
import getContacts  from '../../apiCalls'

import ContactCard from "../ContactCard";

type AppProps = {
    favChange: number;
    setFavChange: (number: number) => void;
}

const ContactList = (props: AppProps) => {
    //state loading manage
    const [loading, setLoading] = useState<boolean>(true)
    //state request error
    const [requestError, setRequestError] = useState<boolean>(false)
    //state contacts -> is used for keep always all contacts
    const [contacts, setContacts] = useState<Array<Data> | undefined>()
    //state contactVisible -> is used for the display
    const [contactsVisible, setContactsVisible] = useState<Array<Data> | undefined>()
    //state button disabled
    const [btnDisabled, setBtnDisabled] = useState<boolean>(false)
    //state searchBar
    const [searchBar, setSearchBar] = useState<string>("")

    //Api call when component did mount
    useEffect(() => {
        const apiCall = async () => {
            const res = await getContacts(1)
            if (res.error) return setRequestError(true)
            setLoading(false)
            setContacts(res)
            setContactsVisible(res)
        }
        apiCall()
        
    },[])

    //contact management visible when the state searchBar changes and when btnDisabled goes to false to filter new contacts
    useEffect(() => {
        const filter = contacts?.filter(elm => {
            const fullname = `${elm.firstname}${elm.lastname}`;
            const filterFullname = fullname.toLocaleLowerCase().includes(searchBar.replace(/\s+/g,"").toLocaleLowerCase())
            const filterEmail = elm.email.toLocaleLowerCase().includes(searchBar.trim().toLocaleLowerCase())
            return filterFullname || filterEmail
        })

        setContactsVisible(filter)
    },[searchBar, btnDisabled])

    useEffect(()=>console.log(contacts))

    //fonction onclick btn showMore
    const _onClick = async () => {
        const res = await getContacts(2)
        if (res.error) return setRequestError(true)

        setContacts([...(contacts ?? []), ...res])
        setBtnDisabled(true)
        
        // if searchBar is empty add directly to contactvisible
        if(!searchBar) setContactsVisible([...(contactsVisible ?? []), ...res])
    }

    return(
        <div className="contact-list-ctn">
            <div className="contact-list-ctn-search">
                <input className="contact-list-search" type="text" placeholder="search" value={searchBar} onChange={(e) => setSearchBar(e.target.value) }/>
                <div className="contact-list-total">Total : {contactsVisible?.length}</div>
            </div>
            {loading && <div>Loading of contacts...</div>}
            {
            !requestError 
                ? 
                contactsVisible?.map( elm => <ContactCard key={elm.id} {...elm} {...props} />)
                :
                <div>An error occurred while loading the page please reload the page</div>
            }
            <div className="contact-list-ctn-btn">
                <button className="contact-list-btn" disabled={btnDisabled} onClick={_onClick}>Show more</button>
            </div>
        </div>
    )
}

export default ContactList;