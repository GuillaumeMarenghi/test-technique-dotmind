import React, {useState, useEffect} from "react";
import './ContactCard.scss'

type AppProps = {
    id: number;
    avatar: string;
    firstname: string;
    lastname: string;
    email: string;
    favChange: number;
    setFavChange: (number:number) => void;
}

const ContactCard = ({id, avatar, firstname , lastname, email, favChange, setFavChange}: AppProps) => {

    const [fav, setFav] = useState<boolean>(false)

    const blackHearts = "https://gist.githubusercontent.com/bltnico/6f69566be9861c6125dd132b78aef6f1/raw/6a0937aeeaf324649b10e39951b6e331fb700720/heart-fill.svg"
    const emptyHearts = "https://gist.githubusercontent.com/bltnico/6f69566be9861c6125dd132b78aef6f1/raw/6a0937aeeaf324649b10e39951b6e331fb700720/heart.svg"
    
    //when component did mount search on local storage if contact is on fav
    useEffect(()=>{
        const isFav = localStorage.getItem(email)
        
        if(isFav) setFav(true)
    },[])

    //function onClick on fav
    const _onClick = () => {
        //load contact on localStorage
        if(!fav) localStorage.setItem(email, JSON.stringify({id, avatar, firstname , lastname, email}));
        //remove contact on local storage
        if(fav) localStorage.removeItem(email)
        //invert fav
        setFav(!fav)
        //change state favChange to allow contactfav to detect the change
        setFavChange(++favChange)
    }

    //useEffect to change hearts icon in contactList when unFav in Favourite list
    useEffect(() => {
        const currentContactFav = localStorage.getItem(email)
        if(!currentContactFav) setFav(false)
    },[favChange])

    return (
        <div className="contact-card-ctn">
            <div className="contact-card-ctn-avatar"><img src={avatar} width={80} height={80}/></div>
            <div className="contact-card-ctn-namespace">
                <div>{firstname}{" "}{lastname}</div>
                <div>{email}</div>
            </div>
            <div className="contact-card-ctn-fav" onClick={_onClick}>{!fav ? <img src={emptyHearts} /> : <img src={blackHearts} /> }</div>
        </div>
    )
}

export default ContactCard;